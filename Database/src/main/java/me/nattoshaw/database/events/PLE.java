package me.nattoshaw.database.events;

import me.nattoshaw.database.Core;
import me.nattoshaw.database.mysql.MySQL;
import me.nattoshaw.database.ranks.RankManager;
import me.nattoshaw.database.utils.ChatUtil;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class PLE implements Listener {

    @EventHandler
    public void onLogin(PostLoginEvent e) {
        ProxiedPlayer player = e.getPlayer();

        MySQL.getInstance().insertPlayerTable(player.getUniqueId().toString(), player.getName(), "DEFAULT");

        for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
            p.sendMessage(new TextComponent(ChatUtil.colorString("&a&l" + p.getName() + " Has joined the server!")));
        }
        if (!(MySQL.getInstance().rankMap.containsKey(player.getName()))) {
            MySQL.getInstance().rankMap.put(player.getName(), RankManager.getInstance().getRank(player.getName()));
        }
        Core.getInstance().getLogger().info(ChatUtil.colorString("&3--------&r" + player.getName().toUpperCase() + "&3--------\n" +
                "                            &3UUID: &r" + player.getUniqueId().toString() + "\n" +
                "                            \n" +
                "                            &3IP: &r" + player.getAddress() + "\n" +
                "                            \n" +
                "                            &3Rank: &r" + RankManager.getInstance().getRank(player.getName()).toUpperCase() + "\n" +
                "                            &3-------------------------"));
            MySQL.getInstance().rankMap.remove(player.getName());
            System.out.println(MySQL.getInstance().rankMap.keySet());
            System.out.println(MySQL.getInstance().rankMap);
    }
}