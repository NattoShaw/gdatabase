package me.nattoshaw.database.mysql;

import me.nattoshaw.database.Core;
import me.nattoshaw.database.utils.FileUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class MySQL {

    public static MySQL getInstance() {
        return Core.getInstance().mySQL;
    }

    private String host;
    private String port;
    private String database;
    private String user;
    private String password;

    public Connection c;

    public ArrayList<String> nameArray;
    public Map<String, String> rankMap;
    public ArrayList<String> rankArray;
    public HashMap<String, Integer> isStaffMap;
    public HashMap<String, Integer> isDonorMap;

    public MySQL() {
        nameArray = new ArrayList<>();
        rankMap =  new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        rankArray = new ArrayList<>();
        isStaffMap = new HashMap<>();
        isDonorMap = new HashMap<>();
        host = FileUtil.getInstance().mysqlConfig.getString("Accounts.host");
        port = FileUtil.getInstance().mysqlConfig.getString("Accounts.port");
        database = FileUtil.getInstance().mysqlConfig.getString("Accounts.database");
        user = FileUtil.getInstance().mysqlConfig.getString("Accounts.user");
        password = FileUtil.getInstance().mysqlConfig.getString("Accounts.password");
    }

    public synchronized void openConnection() {
        try {
            c = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password);
            System.out.println("MySQL: Connected!");
        } catch (SQLException e) {
            System.out.println("Error while connecting to MySQL: ");
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            if (c != null && !c.isClosed()) {
                c.close();
                System.out.println("MySQL: Closed!");
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void createPlayerTable() {
        try {
            PreparedStatement create = c.prepareStatement("CREATE TABLE IF NOT EXISTS playerinfo(PlayerID int AUTO_INCREMENT, UUID varchar(50), Name varchar(30), Rank varchar(30), PRIMARY KEY(PlayerID))");
            create.executeUpdate();
        } catch (SQLException e) {
            System.out.println("MySQL: Could not create Player table, Error:");
            e.printStackTrace();
        }
    }

    public void insertPlayerTable(String UUID, String name, String rank) {
        try {
            getData();
            if (!(getData().contains(name))) {
                PreparedStatement insert = c.prepareStatement("INSERT INTO playerinfo (UUID, Name, Rank) VALUES ('" + UUID + "', '" + name + "', + '" + rank + "')");
                insert.executeUpdate();
                getData().clear();
                return;
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Could not insert into Player table, Error:");
            e.printStackTrace();
        }
    }

    public void updateRank(String name, String rank) {
        try {
            PreparedStatement update = c.prepareStatement("UPDATE playerinfo SET Rank='" + rank.toUpperCase() + "' WHERE Name='" + name + "'");
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getData() {
        try {
            Statement stmt = c.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM playerinfo");
            while (result.next()) {
                nameArray.add(result.getString(3));
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Error while getting data from player, Error:");
            System.out.println(e.getMessage());
        }
        return nameArray;
    }

    public Map<String, String> getDataPlayerRank() {
        try {
            Statement stmt = c.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM playerinfo");
            while (result.next()) {
                rankMap.put(result.getString(3), result.getString(4));
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Error while getting Rank Data, Error:");
            System.out.println(e.getMessage());
        }
        return rankMap;
    }

    public void createRankTable() {
        try {
            PreparedStatement create = c.prepareStatement("CREATE TABLE IF NOT EXISTS rankinfo(Rank VARCHAR(30) , isStaff BOOLEAN, isDonor BOOLEAN)");
            create.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (!(getRankData().contains("DEFAULT"))) {
            insertRankTable("DEFAULT", 0, 0);
        }

        if (!(getRankData().contains("OWNER"))) {
            insertRankTable("OWNER", 1, 0);
        }
    }

    public void insertRankTable(String rank, int isStaff, int isDonor) {
        try {
            PreparedStatement insert = c.prepareStatement("INSERT  INTO rankinfo(Rank, isStaff, isDonor) VALUES ('" + rank + "', '" + isStaff + "', '" + isDonor + "')");
            insert.executeUpdate();
            if (!(rankArray.contains(rank))) {
                rankArray.add(rank.toUpperCase());
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Error while inserting into rank table, Error: ");
            e.printStackTrace();
        }
    }

    public void deleteRank(String rank) {
        try {
            PreparedStatement delete = c.prepareStatement("DELETE FROM rankinfo WHERE Rank= '" + rank.toUpperCase() + "'");
            delete.executeUpdate();
            rankArray.remove(rank.toUpperCase());
            System.out.println(rankArray);
        } catch (SQLException e) {
            System.out.println("MySQL: Error while deleting a rank, Error: ");
            e.printStackTrace();
        }
    }

    public ArrayList<String> getRankData() {
        try {
            Statement stmt = c.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM rankinfo");
            while (result.next()) {
                if (!(rankArray.contains(result.getString(1)))) {
                    rankArray.add(result.getString(1));
                }
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Error while getting data from rank, Error:");
            System.out.println(e.getMessage());
        }
        return rankArray;
    }

    public HashMap<String, Integer> isStaff() {
        try {
            Statement stmt = c.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM rankinfo");
            while (result.next()) {
                isStaffMap.put(result.getString(1), result.getInt(2));
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Error while getting data from rank, Error:");
            System.out.println(e.getMessage());
        }
        return isStaffMap;
    }

    public HashMap<String, Integer> isDonor() {
        try {
            Statement stmt = c.createStatement();
            ResultSet result = stmt.executeQuery("SELECT * FROM rankinfo");
            while (result.next()) {
                isDonorMap.put(result.getString(1), result.getInt(3));
            }
        } catch (SQLException e) {
            System.out.println("MySQL: Error while getting data from rank, Error:");
            System.out.println(e.getMessage());
        }
        return isDonorMap;
    }

    public boolean arrayContains(ArrayList<String> array, Object o) {
        String str = (String) o;
        for (String s : array) {
            if (str.equalsIgnoreCase(s)) {
                return true;
            }
        }
        return false;
    }
}