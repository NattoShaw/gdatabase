package me.nattoshaw.database.commands;

import me.nattoshaw.database.Core;
import me.nattoshaw.database.mysql.MySQL;
import me.nattoshaw.database.ranks.RankManager;
import me.nattoshaw.database.utils.ChatUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class Rank extends Command {

    public Rank() {
        super("rank");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        /***************************Rank Command**********************************/
        if (sender.hasPermission("rank")) {
            if (args.length == 0) {
                sender.sendMessage(new TextComponent(ChatUtil.colorString("&7-------&3Rank Commands&7-------\n" +
                        "&3- /rank set <username> <rank>\n" +
                        "&3- /rank create <rank name> <isStaff> <isDonor>\n" +
                        "&3- /rank delete <rank name>")));
                return;
            }
        } else {
            sender.sendMessage(new TextComponent(ChatUtil.colorString("&cYou do not have enough permission to execute this command!")));
            return;
        }
        /***************************Rank set Command**********************************/
        if (sender.hasPermission("rank.set")) {
            if (args[0].equalsIgnoreCase("set")) {
                if ((args.length == 1 || args.length == 0 || args.length == 2) && (args[0].equalsIgnoreCase("set"))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&c/rank set <username> <rank>")));
                    return;
                }
                ProxiedPlayer player = Core.getInstance().getProxy().getPlayer(args[1]);
                if (player == null) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&cPlayer is offline")));
                    return;
                } else if (!(MySQL.getInstance().arrayContains(MySQL.getInstance().getRankData(), args[2]))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&cRank does not exist!")));
                    return;
                } else if (args[0].equalsIgnoreCase("set") && player != null && MySQL.getInstance().arrayContains(MySQL.getInstance().getRankData(), args[2])) {
                    RankManager.getInstance().setRank(args[2], player);
                    Core.getInstance().getProxy().broadcast(new TextComponent(ChatUtil.colorString("&a" + player + " &3has been set to &a" + args[2].toUpperCase() + " &3rank!")));
                }
            }
        } else {
            sender.sendMessage(new TextComponent(ChatUtil.colorString("&cYou do not have enough permission to execute this command!")));
            return;
        }

        /***************************Rank create Command**********************************/
        if (sender.hasPermission("rank.create")) {
            if (args[0].equalsIgnoreCase("create")) {
                if ((args.length == 1 || args.length == 0 || args.length == 2 || args.length == 3) && (args[0].equalsIgnoreCase("create"))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&c/rank create <rank name> <isStaff> <isDonor>")));
                    return;
                } else if (!(args[2].equalsIgnoreCase("false") || args[2].equalsIgnoreCase("true"))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&cPlease write true or false in the isStaff section")));
                    return;
                } else if (!(args[3].equalsIgnoreCase("false") || args[3].equalsIgnoreCase("true"))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&cPlease write true or false in the isDonor section")));
                    return;
                } else if (MySQL.getInstance().arrayContains(MySQL.getInstance().getRankData(), args[1])) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&cRank already exists!")));
                    return;
                } else if (args[0].equalsIgnoreCase("create") &&
                        (args[3].equalsIgnoreCase("false") ||
                                args[3].equalsIgnoreCase("true") &&
                                        (args[2].equalsIgnoreCase("false") ||
                                                args[2].equalsIgnoreCase("true")))) {
                    MySQL.getInstance().insertRankTable(args[1].toUpperCase(), RankManager.getInstance().isStaffInt(args[2]), RankManager.getInstance().isDonorInt(args[3]));
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&3You have created the &a" + args[1].toUpperCase() + " &3rank!")));
                    return;
                }
            }
        } else {
            sender.sendMessage(new TextComponent(ChatUtil.colorString("&cYou do not have enough permission to execute this command!")));
            return;
        }

        /************************Rank delete Command********************************/
        if (sender.hasPermission("rank.delete")) {
            if (args[0].equalsIgnoreCase("delete")) {
                if ((args.length == 1 || args.length == 0) && (args[0].equalsIgnoreCase("delete"))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&c/rank delete <rank name>")));
                    return;
                } else if (!(MySQL.getInstance().arrayContains(MySQL.getInstance().getRankData(), args[1]))) {
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&cRank does not exist!")));
                    return;
                } else if (args[0].equalsIgnoreCase("delete") && MySQL.getInstance().arrayContains(MySQL.getInstance().getRankData(), args[1])) {
                    MySQL.getInstance().deleteRank(args[1]);
                    sender.sendMessage(new TextComponent(ChatUtil.colorString("&3You have deleted the &a" + args[1].toUpperCase() + " &3rank!")));
                    return;
                }
            }
        } else {
            sender.sendMessage(new TextComponent(ChatUtil.colorString("&cYou do not have enough permission to execute this command!")));
            return;
        }
    }
}
