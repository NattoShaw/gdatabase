package me.nattoshaw.database.commands;

import me.nattoshaw.database.utils.ChatUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

/**
 * Copyright All right reserved © NattoShaw.
 */

public class CommandBungee extends Command {
    public CommandBungee() {
        super("bungee");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        sender.sendMessage(new TextComponent(ChatUtil.colorString("&9This server is running BungeeCord version NathsTestServer &9by md_5")));
    }
}