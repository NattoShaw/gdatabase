package me.nattoshaw.database.commands;

import me.nattoshaw.database.mysql.MySQL;
import me.nattoshaw.database.ranks.RankManager;
import me.nattoshaw.database.utils.ChatUtil;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class Info extends Command {

    public Info() {
        super("info");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (sender instanceof ProxiedPlayer) {
            if (args.length == 0) {
                if (!(MySQL.getInstance().rankMap.containsKey(sender.getName()))) {
                    MySQL.getInstance().rankMap.put(sender.getName(), RankManager.getInstance().getRank(sender.getName()));
                }
                ProxiedPlayer playerSender = (ProxiedPlayer) sender;
                String getRank = RankManager.getInstance().getRank(playerSender.getName()).toUpperCase();
                sender.sendMessage(new TextComponent(ChatUtil.colorString("&3-----&a" + sender.getName().toUpperCase() + "&3-----\n" +
                        "&aRank: &3" + getRank + "\n" +
                        "&aLevel: &cCOMING SOON \n" +
                        "&aStaff: &3" + RankManager.getInstance().isStaff(getRank) + "\n" +
                        "&aDonor: &3" + RankManager.getInstance().isDonor(getRank))));

                if (MySQL.getInstance().rankMap.containsKey(sender.getName())) {
                    MySQL.getInstance().rankMap.remove(sender.getName());
                    System.out.println(MySQL.getInstance().rankMap);
                    return;
                }
                return;
            }
            if (MySQL.getInstance().getDataPlayerRank().get(args[0]) == null) {
                sender.sendMessage(new TextComponent(ChatUtil.colorString("&cPlayer has never joined the server!")));
                return;
            }
            if (!(MySQL.getInstance().rankMap.containsKey(args[0]))) {
                MySQL.getInstance().rankMap.put(args[0], RankManager.getInstance().getRank(args[0]));
            }
            String getMyRank = RankManager.getInstance().getRank(args[0]);
            sender.sendMessage(new TextComponent(ChatUtil.colorString("&3-----&a" + args[0].toUpperCase() + "&3-----\n" +
                    "&aRank: &3" + getMyRank + "\n" +
                    "&aLevel: &cCOMING SOON \n" +
                    "&aStaff: &3" + RankManager.getInstance().isStaff(getMyRank) + "\n" +
                    "&aDonor: &3" + RankManager.getInstance().isDonor(getMyRank))));

            if (MySQL.getInstance().rankMap.containsKey(args[0])) {
                MySQL.getInstance().rankMap.remove(args[0]);
                System.out.println(MySQL.getInstance().rankMap);
                return;
            }
            return;
        } else {
            if (args.length == 0 && (!(sender instanceof ProxiedPlayer))) {
                sender.sendMessage(new TextComponent(ChatUtil.colorString("&cPlease enter a player name!")));
                return;
            }

            if (MySQL.getInstance().getDataPlayerRank().get(args[0]) == null) {
                sender.sendMessage(new TextComponent(ChatUtil.colorString("&cPlayer has never joined the server!")));
                return;
            }
            if (!(MySQL.getInstance().rankMap.containsKey(args[0]))) {
                MySQL.getInstance().rankMap.put(args[0], RankManager.getInstance().getRank(args[0]));
            }
            String getMyRank = RankManager.getInstance().getRank(args[0]);
            sender.sendMessage(new TextComponent(ChatUtil.colorString("&3-----&a" + args[0].toUpperCase() + "&3-----\n" +
                    "&aRank: &3" + getMyRank + "\n" +
                    "&aLevel: &cCOMING SOON \n" +
                    "&aStaff: &3" + RankManager.getInstance().isStaff(getMyRank) + "\n" +
                    "&aDonor: &3" + RankManager.getInstance().isDonor(getMyRank))));

            if (MySQL.getInstance().rankMap.containsKey(args[0])) {
                MySQL.getInstance().rankMap.remove(args[0]);
                System.out.println(MySQL.getInstance().rankMap);
                return;
            }
        }
    }
}
