package me.nattoshaw.database;

import me.nattoshaw.database.commands.CommandBungee;
import me.nattoshaw.database.commands.Info;
import me.nattoshaw.database.commands.Rank;
import me.nattoshaw.database.events.PLE;
import me.nattoshaw.database.mysql.MySQL;
import me.nattoshaw.database.ranks.RankManager;
import me.nattoshaw.database.utils.ChatUtil;
import me.nattoshaw.database.utils.FileUtil;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class Core extends Plugin {

    private static Core instance;
    public MySQL mySQL;
    public RankManager rm;
    public FileUtil fileUtil;

    public void onLoad() {
        getLogger().info(ChatUtil.colorString("&a--------&3&lDatabse&a--------\n" +
                "                           \n" +
                "                           &aEnabling!\n" +
                "                           \n" +
                "                           &aVersion: " + getDescription().getVersion() + "\n" +
                "                           &a&l-----------------------"));
    }

    public void onEnable() {
        instance = this;
        fileUtil = new FileUtil();
        fileUtil.setup();
        mySQL = new MySQL();
        mySQL.openConnection();
        rm = new RankManager();
        getLogger().info(ChatUtil.colorString("&a--------&3Databse&a--------\n" +
                "                           \n" +
                "                           &aEnabled!\n" +
                "                           \n" +
                "                           &aVersion: " + getDescription().getVersion() + "\n" +
                "                           &a&l-----------------------"));
        mySQL.createPlayerTable();
        mySQL.createRankTable();
        registerEvents();
        registerCommands();

        System.out.println(MySQL.getInstance().isStaff().keySet() + " || " + MySQL.getInstance().isStaff().values());
        System.out.println(MySQL.getInstance().isDonor().keySet() + " || " + MySQL.getInstance().isDonor().values());
    }

    public void onDisable() {
        instance = null;
        fileUtil.saveMysqlFile();
        mySQL.closeConnection();
        getLogger().info(ChatUtil.colorString("&c--------&3&lDatabse&c--------\n" +
                "                           \n" +
                "                           &cDisabled!\n" +
                "                           \n" +
                "                           &cVersion: " + getDescription().getVersion() + "\n" +
                "                           &c&l-----------------------"));
    }

    private void registerEvents() {
        PluginManager pm = getProxy().getPluginManager();
        pm.registerListener(this, new PLE());
    }

    private void registerCommands() {
        PluginManager pm = getProxy().getPluginManager();
        pm.registerCommand(this, new Rank());
        pm.registerCommand(this, new CommandBungee());
        pm.registerCommand(this, new Info());
    }

    public static Core getInstance() {
        return instance;
    }
}