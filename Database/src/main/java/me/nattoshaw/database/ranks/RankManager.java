package me.nattoshaw.database.ranks;

import me.nattoshaw.database.Core;
import me.nattoshaw.database.mysql.MySQL;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class RankManager {

    public static RankManager getInstance() {
        return Core.getInstance().rm;
    }

    public RankManager() {

    }

    public void setRank(String rank, ProxiedPlayer player) {
        MySQL.getInstance().rankMap.remove(player.getName());
        MySQL.getInstance().updateRank(player.getName(), rank);
    }

    public String getRank(String name) {
        return MySQL.getInstance().getDataPlayerRank().get(name);
    }

    public String isStaff(String rank) {
        if (MySQL.getInstance().isStaff().get(rank) == 1) {
            return "True";
        }
        return "False";
    }

    public String isDonor(String rank) {
        if (MySQL.getInstance().isDonor().get(rank) == 1) {
            return "True";
        }
        return "False";
    }

    public int isStaffInt(String string) {
        if (string.equalsIgnoreCase("true")) {
            return 1;
        } else if (string.equalsIgnoreCase("false")) {
            return 0;
        }
        return 0;
    }

    public int isDonorInt(String string) {
        if (string.equalsIgnoreCase("true")) {
            return 1;
        } else if (string.equalsIgnoreCase("false")) {
            return 0;
        }
        return 0;
    }
}