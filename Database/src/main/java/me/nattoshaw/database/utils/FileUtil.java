package me.nattoshaw.database.utils;

import com.google.common.io.ByteStreams;
import me.nattoshaw.database.Core;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class FileUtil {

    public static FileUtil getInstance (){
        return Core.getInstance().fileUtil;
    }

    public Configuration mysqlConfig;
    public ConfigurationProvider mysqlCP;
    public File mysqlFile;


    public void setup() {
        loadMySQLFile();
        if (!Core.getInstance().getDataFolder().exists()) {
            Core.getInstance().getDataFolder().mkdir();
        }
        mysqlFile = new File(Core.getInstance().getDataFolder(), "MySQL.yml");

        if (!mysqlFile.exists()) {
            try {
                mysqlFile.createNewFile();
                try (InputStream is = Core.getInstance().getResourceAsStream("MySQL.yml");
                     OutputStream os = new FileOutputStream(mysqlFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                System.out.println(" [ERROR]: Could not create a new MySQL file: ");
                e.printStackTrace();
            }
        }
    }

    public void loadMySQLFile() {
        try {
            mysqlConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(Core.getInstance().getDataFolder(), "MySQL.yml"));
        } catch (IOException e) {
            System.out.println(" [ERROR]: Could not load MySQL file: ");
            e.printStackTrace();
        }
    }

    public void saveMysqlFile() {
        try {
            mysqlCP.getProvider(YamlConfiguration.class).save(mysqlConfig, new File(Core.getInstance().getDataFolder(), "MySQL.yml"));
        } catch (IOException e) {
            System.out.println(" [ERROR]: Could not save MySQL file: ");
            e.printStackTrace();
        }
    }

    public void reloadMySQLFile() {
        loadMySQLFile();
    }
}