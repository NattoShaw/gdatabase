package me.nattoshaw.database.utils;

import net.md_5.bungee.api.ChatColor;

/**
 * Copyright All right reserved © NattoShaw.
 */
public class ChatUtil {
    public static String colorString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

}
